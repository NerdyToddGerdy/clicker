from tkinter import BOTH, RAISED, TOP, Tk
from tkinter.ttk import Button, Frame, Style, Label
from tier_levels import TIERS
import logging

from Tier import Tier


class MainGui(Frame):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.total_count = 0
        parent.title('"Todd Gerdy\'s Shut Up and Click: Gaiden"')
        # self.style = Style
        # self.style.theme_use("default")
        frame = Frame(self, relief=RAISED, borderwidth=1)
        frame.pack(fill=BOTH, expand=True)
        self.pack(fill=BOTH, expand=True)
        logging.info('Main Frame created')
        self.main_counter = Label(self, text=self.total_count)
        self.main_counter.pack(fill=BOTH)
        logging.info('Clicker button created')
        for tier in TIERS:
            print(tier)
            print(tier['button_name'])
            Tier(
                self,
                button_name=tier['button_name'],
                max_bytes=tier['max_bytes'],
                read_bytes_size=tier['read_bytes_size'],
                next_level=tier['next_level'],
                increment_amount=tier['increment_amount']
            )

    def subtract(self, amount):
        self.total_count -= amount
        self.main_counter.config(text=self.total_count)

    def increment(self, increment_amount: int):
        print("clicking")
        self.total_count += increment_amount
        self.main_counter.config(text=self.total_count)


def main():
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    logging.info('Started')

    root = Tk()
    gui = MainGui(root)
    root.mainloop()

    logging.info('Finished')

if __name__ == "__main__":
    main()
