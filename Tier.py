#!/usr/bin/python3
# -*- coding: utf-8 -*-


from tkinter import (BOTH, CENTER, DISABLED, LEFT, NORMAL, RAISED, RIGHT, TOP,
                     Tk)
from tkinter.ttk import Button, Frame, Label, Progressbar, Style


class Tier(Frame):
    """
        This is a class for a tier of the upgrade process.
    """
    def __init__(
        self,
        parent,
        button_name: str,
        max_bytes: int,
        read_bytes_size: int,
        next_level: int,
        increment_amount: int
    ):
        """
            The constructor for the Tier class.

            Parameters:
                parent (class): This is the parent class.
                button_name (str): This is the name that will be displayed on
                    the button
                total (int): the total count of entire game.
        """
        super().__init__()
        self.button_name = button_name
        self.parent = parent
        self.bytes = read_bytes_size
        self.maxbytes = max_bytes
        self.upgrade_cost = 0
        self.auto = False
        self.next_level = next_level
        self.increment_amount = increment_amount
        self.initUI()
        # self.after(1000, self.check_for_upgrade())
        self.timer_event = self.after(1000, self.check_for_upgrade)
        # self.after_cancel(self.timer_event)
        self.parent.after(1000, self.check_for_upgrade)

    def initUI(self):
        """
            This creates the User Interface for this Frame.
        """
        self.style = Style()
        self.style.theme_use("default")
        self.level_count = 1

        frame = Frame(self, relief=RAISED, borderwidth=1)
        frame.pack(fill=BOTH, expand=True)
        self.pack(fill=BOTH, expand=True)

        self.label = Label(self, text=f"next upgrade: {self.next_level}")
        self.label.pack(side=LEFT, padx=5, pady=5)
        self.level = Label(self, text=f"current level: {self.level_count}")
        self.level.pack(side=LEFT)

        self.progressbar = Progressbar(
            self,
            orient="horizontal",
            length=200,
            mode="determinate"
        )
        self.progressbar.pack(side=RIGHT)

        self.upgrade_button = Button(
            self,
            text="Upgrade",
            state=DISABLED,
            command=self.upgrade)
        self.upgrade_button.pack(side=RIGHT, padx=5)

        self.increment_button = Button(
            self,
            text=self.button_name,
            command=self.increment)
        self.increment_button.pack(side=RIGHT)

    def increment(self):
        # self.parent.increment(self.increment_amount)
        self.progressbar["value"] = 0
        self.progressbar["maximum"] = self.maxbytes
        self.check_for_upgrade()
        self.read_bytes()

    def upgrade(self):
        self.increment_amount *= 2
        self.parent.subtract(self.next_level)
        self.next_level *= 2
        self.label.config(text=f"next upgrade: {self.next_level}")
        self.level_count += 1
        self.level.config(text=f"current level: {self.level_count}")

    def read_bytes(self):
        self.bytes += 500
        self.progressbar["value"] = self.bytes
        if self.bytes < self.maxbytes:
            self.after(100, self.read_bytes)
        else:
            self.bytes = 0
            self.progressbar["value"] = self.bytes
            self.parent.increment(self.increment_amount)
            if self.auto:
                self.after(100, self.increment)

    def check_for_upgrade(self):
        if self.parent.total_count >= self.next_level:
            # print('upgradeable')
            self.upgrade_button.config(state=NORMAL)
        else:
            self.upgrade_button.config(state=DISABLED)
        self.parent.after(1000, self.check_for_upgrade)
